# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        maxdistance = newFood.width + newFood.height
        minFoodDistance = 999999
        for i in range(newFood.width):
            for j in range(newFood.height):
                if newFood[i][j] == True:
                    minFoodDistance = min(minFoodDistance, 
                                          manhattanDistance(newPos, (i, j)))
        distance = 0
        for index in range(len(newScaredTimes)):
            absDistance = manhattanDistance(newPos, 
                                         newGhostStates[index].getPosition())
            if newScaredTimes[index] > 0:
                if absDistance < 4:
                    distance += maxdistance - absDistance
            else:
                if absDistance < 3:
                    distance += absDistance - 3
        totalScore = 2 * distance - newFood.count(True) + 1.0 / minFoodDistance
        return totalScore

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def maxValue(self, gameState, agentIndex, currentDepth):
        maxEval = -999999
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        for action in gameState.getLegalActions(agentIndex):
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, 
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            if maxEval < nodeEval[1]:
                agentActions = action
                maxEval = nodeEval[1]
        return [agentActions, maxEval]

    def minValue(self, gameState, agentIndex, currentDepth):
        minEval = 999999
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        for action in gameState.getLegalActions(agentIndex):
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, 
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            if minEval > nodeEval[1]:                
                agentActions = action
                minEval = nodeEval[1]
        return [agentActions, minEval]

    def nodeValue(self, gameState, agentIndex, currentDepth):
        if agentIndex == 0:
            if currentDepth == self.depth:
                score = self.evaluationFunction(gameState)
                return [0, score]
            return self.maxValue(gameState, 0, currentDepth + 1)
        else:
            return self.minValue(gameState, agentIndex, currentDepth)

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        v = self.nodeValue(gameState, 0, 0)
        return v[0]


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        alpha = -999999
        beta = 999999
        v = self.nodeValue(gameState, alpha, beta, 0, 0)
        return v[0]

    def maxValue(self, gameState, alpha, beta, agentIndex, currentDepth):
        maxEval = -999999
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        for action in gameState.getLegalActions(agentIndex):
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, alpha, beta,
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            if maxEval < nodeEval[1]:
                agentActions = action
                maxEval = nodeEval[1]
            if maxEval > beta:
                return [agentActions, maxEval]
            alpha = max(alpha, maxEval)
        return [agentActions, maxEval]

    def minValue(self, gameState, alpha, beta, agentIndex, currentDepth):
        minEval = 999999
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        for action in gameState.getLegalActions(agentIndex):
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, alpha, beta, 
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            if minEval > nodeEval[1]:                
                agentActions = action
                minEval = nodeEval[1]
            if minEval < alpha:
                return [agentActions, minEval]
            beta = min(beta, minEval)
        return [agentActions, minEval]

    def nodeValue(self, gameState, alpha, beta, agentIndex, currentDepth):
        if agentIndex == 0:
            if currentDepth == self.depth:
                score = self.evaluationFunction(gameState)
                return [0, score]
            return self.maxValue(gameState, alpha, beta, 0, currentDepth + 1)
        else:
            return self.minValue(gameState, alpha, beta, agentIndex, currentDepth)

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        v = self.nodeValue(gameState, 0, 0)
        return v[0]

    def maxValue(self, gameState, agentIndex, currentDepth):
        maxEval = -999999
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        for action in gameState.getLegalActions(agentIndex):
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, 
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            if maxEval < nodeEval[1]:
                agentActions = action
                maxEval = nodeEval[1]
        return [agentActions, maxEval]

    def expectValue(self, gameState, agentIndex, currentDepth):
        agentActions = []
        if len(gameState.getLegalActions(agentIndex)) == 0:
            return [0, self.evaluationFunction(gameState)]
        totalEval = 0
        count = 0
        for action in gameState.getLegalActions(agentIndex):
            count += 1
            successorState = gameState.generateSuccessor(agentIndex, action)
            nodeEval = self.nodeValue(successorState, 
                                      (agentIndex + 1) % gameState.getNumAgents(),
                                      currentDepth)
            totalEval += nodeEval[1]
        return [0, float(totalEval)/float(count)]

    def nodeValue(self, gameState, agentIndex, currentDepth):
        if agentIndex == 0:
            if currentDepth == self.depth:
                score = self.evaluationFunction(gameState)
                return [0, score]
            return self.maxValue(gameState, 0, currentDepth + 1)
        else:
            return self.expectValue(gameState, agentIndex, currentDepth)        

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    from searchAgents import mazeDistance
    from game import Directions
    newPos = currentGameState.getPacmanPosition()
    newFood = currentGameState.getFood()
    newGhostStates = currentGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
    capsules = currentGameState.getCapsules()
    maxDistance = newFood.width + newFood.height
    minFoodDistance = 999999
    minCapsuleDistance = 999999

    for capsulePos in capsules:
        capsuleDistance = mazeDistance(newPos, capsulePos, currentGameState)
        if capsuleDistance < minCapsuleDistance:
            minCapsuleDistance = capsuleDistance
    if minCapsuleDistance > 0:
        minCapsuleDistance = 1.0 / minCapsuleDistance

    for i in range(newFood.width):
        for j in range(newFood.height):
            if newFood[i][j] == True:
                minFoodDistance = min(minFoodDistance, 
                                      manhattanDistance(newPos, (i, j)))

    distance = 0
    for index in range(len(newScaredTimes)):
        absDistance = manhattanDistance(newPos, newGhostStates[index].getPosition())
        if newScaredTimes[index] > 0:
            distance += 1.0 / absDistance
           # if absDistance < 2:
            #    distance += maxDistance - absDistance
        else:
            if absDistance < 2:
                distance += absDistance - 5

    totalScore = distance - newFood.count(True) + 0.5 / minFoodDistance + minCapsuleDistance
    return totalScore


# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
    """
      Your agent for the mini-contest
    """

    def getAction(self, gameState):
        """
          Returns an action.  You can use any method you want and search to any depth you want.
          Just remember that the mini-contest is timed, so you have to trade off speed and computation.

          Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
          just make a beeline straight towards Pacman (or away from him if they're scared!)
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

