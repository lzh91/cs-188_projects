# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def itemMapPriority(item):
    state = item[0]
    actions = item[1]
    problem = item[2]
    heuristic = item[3]
    return problem.getCostOfActions(actions) + heuristic(state[0], problem)

def chooseStrategy(searchMethod):
    if (searchMethod == 0):
        return util.Stack()
    elif (searchMethod == 1):
        return util.Queue()
    elif (searchMethod == 2):
        return util.PriorityQueueWithFunction(itemMapPriority)
    elif (searchMethod == 3):
        return util.PriorityQueueWithFunction(itemMapPriority)

def genericSearch(problem, searchMethod, heuristic=nullHeuristic):
    """
    generic search method for dfs, bfs. Argument searchMethod denotes the type of the search method: 0 for dfs, 1 for bfs, 2 for ucs ...
    """

    stateContainer = chooseStrategy(searchMethod)
    startState = problem.getStartState()
    successors = problem.getSuccessors(startState)
    for successor in successors:
        stateContainer.push([successor, [successor[1]], problem, heuristic])
    visitedState = {}
    visitedState[startState] = 0
    while (True):
        if (stateContainer.isEmpty()):
            return []
        stateTuple = stateContainer.pop()
        currentState, action, stepCost = stateTuple[0]
        if (visitedState.has_key(currentState)):
            continue
        visitedState[currentState] = True
        if (problem.isGoalState(currentState)):
            break
        successors = problem.getSuccessors(currentState)

        for successor in successors:
            if (not visitedState.has_key(successor[0])):
                actions = stateTuple[1][:]
                actions.append(successor[1])
                stateContainer.push([successor, actions, problem, heuristic])

    return stateTuple[1]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    return genericSearch(problem, 0)

def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first.
    """
    "*** YOUR CODE HERE ***"
    return genericSearch(problem, 1)
    

def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"
    return genericSearch(problem, 2)


def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"
    return genericSearch(problem, 3, heuristic)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
